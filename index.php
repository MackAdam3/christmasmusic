<?php ?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
    <link href="http://vjs.zencdn.net/6.4.0/video-js.css" rel="stylesheet">
    <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
    <script type="text/javascript">
        $(function() {
            var $music = $('audio').get(0);
            $(document).on('keypress', function(e) {
                e.preventDefault();
                console.log(e.charCode);
                switch(e.charCode) {
                    case 97:
                        $music.currentTime = $music.currentTime - 10;
                        break;
                    case 115:
                        if($music.paused) {
                            $music.play();
                        } else {
                            $music.pause();
                        }
                        break;
                    case 100:
                        $music.currentTime = $music.currentTime + 10;
                        break;
                    case 32:
                        $music.currentTime = getRandomInt(0, $music.duration);
                        break;
                }
                return false;
            });

            function getRandomInt(min, max) {
                min = Math.ceil(min);
                max = Math.floor(max);
                return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
            }

            var animation = null;

            $(document).on('mouseover', function(e) {
                e.preventDefault();
                if(animation) {
                    return false;
                }
                animation = $('#keymap').animate({
                    opacity: 1,
                    width: "250px"
                }, 1000, function() {
                    animation = null;
                });
                $('#bgmask').animate({opacity: 1}, 1000);
                return false;
            });

            $(document).on('mouseout', function(e) {
                e.preventDefault();
                if(animation) {
                   return false;
                }
                animation = $('#keymap').animate({
                    opacity: 0,
                    width: "150px"
                }, 1000, function() {
                    animation = null;
                });
                $('#bgmask').animate({opacity: 0}, 1000);
                return false;
            });

        });
    </script>
    <style>
        html,body {
            margin: 0;
            background: #000;
        }
        #fireloop {
            width: 100%;
        }
        #keymap {
            position: absolute;
            top: 50%;
            left: 50%;
            z-index: 9999;
            transform: translate(-50%, -50%);
            width: 150px;
            opacity: 0;
        }
        #bgmask {
            opacity: 0;
            height: 100%;
            width: 100%;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            background-color: rgb(0,0,0);
            background-color: rgba(0,0,0,0.6);
        }
    </style>
</head>
<body>
    <img src="keymap.png" id="keymap">
    <div id="bgmask"></div>
    <video id="fireloop" class="video-js" preload="auto" autoplay loop poster="fire.png" data-setup="{}">
        <source src="fire.mp4" type='video/mp4'>
        <source src="fire.webm" type='video/webm'>
        <p class="vjs-no-js">
            To view this video please enable JavaScript, and consider upgrading to a web browser that
            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
        </p>
    </video>
    <audio preload="auto" autoplay loop src="music.mp3">
    <script src="http://vjs.zencdn.net/6.4.0/video.js"></script>
</body>
</html>
